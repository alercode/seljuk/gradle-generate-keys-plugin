package com.selcuk.depo.manager;

public final class Deps {
    public static final String commonsIO        = "commons-io:commons-io:2.6";
    public static final String eventBus         = "org.greenrobot:eventbus:3.1.1";
    public static final String lombok           = "org.projectlombok:lombok:1.18.10";
    public static final String checkerFramework = "org.checkerframework:checker:3.0.1";

    public static final class Apache {
        private static final String COMMONS           = "org.apache.commons:";
        public static final  String commonsLang       = COMMONS + "commons-lang3:3.9";
        public static final  String commonsCollection = COMMONS + "commons-collections4:4.4";
        public static final  String commonsText       = COMMONS + "commons-text:1.8";
        public static final  String commonsMath       = COMMONS + "commons-math3:3.6.1";
        public static final  String commonsCsv        = COMMONS + "commons-csv:1.7";
        public static final  String commonsExec       = COMMONS + "commons-exec:1.3";
    }

    public static final class Google {
        public static final String guava = "com.google.guava:guava:28.0-jre";
    }

    public static final class JUnit {
        public static final String junit4 = "junit:junit:4.12";
    }
}
