package com.selcuk.depo.manager;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class FxResPlugin implements Plugin<Project> {

    @Override
    public void apply(Project target) {
        target.getTasks().register("processFxRes", ResourceHelper.class);
    }
}
