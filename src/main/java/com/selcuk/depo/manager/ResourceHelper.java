package com.selcuk.depo.manager;


import com.google.common.base.CaseFormat;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;
import org.gradle.api.DefaultTask;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.tasks.TaskAction;

import javax.lang.model.element.Modifier;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ResourceHelper extends DefaultTask {
    private SourceSet sourceSet;

    @TaskAction
    void toTask() {
        getLogger().log(LogLevel.INFO, "Gooooooonnnn starting up....");
        getProject().getPlugins().withType(JavaPlugin.class, javaPlugin -> {
            SourceSetContainer sourceSets = (SourceSetContainer)
                    getProject().getProperties().get("sourceSets");
            sourceSet = sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME);
            File stringsFile = findStringsFile(sourceSet);
            if (stringsFile != null) {
                getLogger().log(LogLevel.INFO, "found strings file");
                writeClass(stringsFile, sourceSet);
            } else {
                getLogger().log(LogLevel.INFO, "Cannot find strings file");
            }
            Set<File> layouts = findLayouts(sourceSet);
            if (layouts.size() > 0) {
                writeLayoutFile(layouts, sourceSet);
            } else {
                getLogger().info("Layout directory not found");
            }
        });
    }

    private Set<File> findLayouts(SourceSet sourceSet) {
        Set<File> files       = sourceSet.getResources().getFiles();
        Set<File> layoutFiles = new HashSet<>();
        for (File file : files) {
            if (file.getName().endsWith(".fxml")) {
                layoutFiles.add(file);
            }
        }
        return layoutFiles;
    }

    private void writeLayoutFile(Set<File> files, SourceSet sourceSet) {
        List<FieldSpec> fieldSpecs = new ArrayList<>();
        Set<File>       res        = sourceSet.getResources().getSrcDirs();
        File            resFile    = new File("ac");
        for (File file : res) {
            resFile = file;
            break;
        }
        for (File file : files) {
            String name = file.getName()
                    .replace(".fxml", "")
                    .replace(' ', '_');

            fieldSpecs.add(FieldSpec.builder(String.class, name,
                    Modifier.FINAL, Modifier.STATIC, Modifier.PUBLIC)
                    .initializer("\"" + file.getPath().replace(resFile.getPath(), "").replace("\\", "/") + "\"")
                    .build());
        }
        TypeSpec typeSpec = TypeSpec.classBuilder("Layouts")
                .addModifiers(Modifier.PUBLIC)
                .addFields(fieldSpecs)
                .build();
        JavaFile javaFile = JavaFile.builder(getProject().getGroup() + ".generated", typeSpec)
                .build();
        File file = new File(sourceSet.getJava().getSourceDirectories().getAsPath());
        try {
            javaFile.writeTo(file);
            sourceSet.getJava().srcDir(file);
        } catch (IOException e) {
            getLogger().error("failed to write layout file because  " + e.getLocalizedMessage());
        }
    }

    private void writeClass(File stringsFile, SourceSet sourceSet) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(stringsFile));
            String         currentLine;
            List<String>   keys   = new ArrayList<>();
            while ((currentLine = reader.readLine()) != null) {
                String key = getKeyFromLine(currentLine);
                if (key != null) {
                    keys.add(key);
                }
            }
            List<FieldSpec> fieldSpecs = new ArrayList<>();
            for (String key : keys) {
                String fieldName = key.replace(".", "_");
                fieldName = CaseFormat.LOWER_CAMEL.converterTo(CaseFormat.UPPER_UNDERSCORE).convert(fieldName);
                getLogger().info("Field name is {}", fieldName);
                if (fieldName != null) {
                    FieldSpec fieldSpec = FieldSpec.builder(String.class, fieldName,
                            Modifier.FINAL, Modifier.STATIC, Modifier.PUBLIC)
                            .initializer("\"" + key + "\"")
                            .build();
                    fieldSpecs.add(fieldSpec);
                }
            }
            TypeSpec typeSpec = TypeSpec.classBuilder("Messages")
                    .addModifiers(Modifier.PUBLIC)
                    .addFields(fieldSpecs)
                    .build();
            JavaFile javaFile = JavaFile.builder(getProject().getGroup() + ".generated", typeSpec)
                    .build();
            File file = new File(sourceSet.getJava().getSourceDirectories().getAsPath());
            javaFile.writeTo(file);
            sourceSet.getJava().srcDir(file);
        } catch (Exception e) {
            getLogger().error("failed to read strings from strings files because " + e.getLocalizedMessage());
        }
    }

    private String getKeyFromLine(String line) {
        int index = line.indexOf("=");
        if (index > -1) {
            String possibleKey = line.substring(0, index);
            if (possibleKey.length() > 0) {
                return possibleKey;
            }
            return null;
        }
        return null;
    }

    private File findStringsFile(SourceSet sourceSet) {
        Set<File> files = sourceSet.getResources().getFiles();
        for (File file : files) {
            if (file.getName().contains("strings")) {
                return file;
            }
        }
        return null;
    }
}